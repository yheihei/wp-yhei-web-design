# 本テーマについて
本テーマは  
https://ja.wordpress.org/themes/draft-portfolio/  
の子テーマです。  
デザイナーのポートフォリオに特化したテーマをさらにカスタマイズして見やすくしています。

# インストール方法

## Draft Portfolioテーマのマスターをインストール
下記の2種類の方法があります  

* https://ja.wordpress.org/themes/draft-portfolio/ からダウンロードしてzipでアップロードする
* WordPress管理画面のテーマインストール画面から検索する


## 本子テーマのインストール

```
$ cd wp-content/themes/
$ git clone https://<your bit bucket account>@bitbucket.org/yheihei/wp-yhei-web-design.git draft-portfolio_child
```

